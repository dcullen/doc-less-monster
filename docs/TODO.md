# TODO

- Then make a shared .tex file containing shared data that that both example files can import.

  How do we implement the shared import? Maybe one of these links will help:
  https://tex.stackexchange.com/questions/383051/importing-tex-file-in-latex
  https://tex.stackexchange.com/questions/11311/how-to-include-a-document-into-another-document
