# Design Decisions and Rationales

- Use Pandoc directly, rather than something like Bookdown, because we don't need all the complexity of introducing R.

- We need to install some additional texlive packages, namely 'lastpage' and 'multirow',
  and some additional fonts such as 'merriweather'. According to section
  [Building custom images in the README.md](https://github.com/pandoc/dockerfiles/blob/master/README.md),
  they recommend extending the standard Pandoc container image by writing your own Dockerfile,
  which installs additional packages. We use the 'tlmgr' command to install additional packages,
   as is done in [install-tex-pacakges.sh](https://github.com/pandoc/dockerfiles/blob/master/common/latex/install-tex-packages.sh).
