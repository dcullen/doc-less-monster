#!/usr/bin/env bash
# run.sh contains commands to set up dependencies and build the documents.

set -euf -o pipefail
SCRIPTNAME=${BASH_SOURCE[0]}
SCRIPTDIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
SUCCESS=0
FAILURE=1

if [ "$OSTYPE" = "msys" ] ; then
    echo "$SCRIPTNAME: Error: Windows 10 MSYS Git Bash shell is unsupported. Please use Ubuntu 20.04 instead."
    echo "Reason: Docker Desktop / WSL2 expects Windows-style paths such as C:\\some\\path"
    echo "and cannot handle MSYS-style paths such as /c/some/path."
    exit $FAILURE
fi

do-all()
{
    do-build "$@"
    do-view "$@"
}

do-help()
{
    echo
    echo "Usage: ./run.sh build <DOCUMENT-PATH>/<DOCUMENT-NAME>"
    echo
    echo "Remarks:"
    echo
    echo "- DOCUMENT-PATH may be either absolute or relative."
    echo
    echo "- DOCUMENT-NAME specifies two input files:"
    echo "  - DOCUMENT-NAME.md: Markdown file containing the body of your document."
    echo "  - DOCUMENT-NAME.tex: LaTeX document containing header information for your document."
    echo
    echo "- In other words, DOCUMENT-NAME must not contain the .md or .tex file extensions."
    echo
    echo "- The output PDF will be safed in a file named '<DOCUMENT-NAME>.pdf'."
    echo
    echo "- Before you can build anything, do 'run deps' to build all project dependencies."
    echo
    echo "- To build the examples, run 'make examples'."
    echo
    echo "- See README.md for more information."
    echo
    echo "- Tip: Add this directory to your PATH or define 'alias run=./run.sh'"
    echo "  in your bashrc to avoid having to type the leading './' every time."
    echo
    echo "Build environment info:"
    echo
    echo "  OSTYPE:              $OSTYPE"
    echo "  PATH:                $PATH"
    echo "  SHELL:               $SHELL"
    echo "  PWD:                 $PWD"
    echo "  SCRIPTNAME:          $SCRIPTNAME"
    echo "  SCRIPTDIR:           $SCRIPTDIR"
    echo
    echo "Software version: $(git describe --match=DoNotMatchAnyTags --always --abbrev=40 --dirty)"
    echo
    echo "Date and time: $(date +%Y-%m-%d-%H%M)"
    echo
}

do-clean()
{
    git clean -d -f -x
}

do-deps()
{
    echo "$SCRIPTNAME: Building Docker container."
    docker build --tag=pandoc-docless:latest .
}

do-build()
{
    if [ $# -lt 1 ] ; then
        echo "$SCRIPTNAME: Error: No document path specified. Run `./run.sh help` for usage."
        exit $FAILURE
    fi

    DIRNAME="$(dirname $1)"
    DOCNAME="$(basename $1)"

    echo "$SCRIPTNAME: Input document: DIRNAME=$DIRNAME, DOCNAME=$DOCNAME"
    
    echo "$SCRIPTNAME: Generating PDF document."
    (
        cd "$DIRNAME"
        docker run --rm --volume "$(pwd)":/data --user "$(id -u):$(id -g)" pandoc-docless:latest "$DOCNAME".md --from=markdown --include-in-header="$DOCNAME".tex -o "$DOCNAME".pdf
    )
}

do-examples()
{
    echo "$SCRIPTNAME: Building examples."
    build examples/development-plan
    build examples/technical-report
}

do-debug-shell()
{
    echo "$SCRIPTNAME: Starting shell inside our Pandoc container so you can inspect the LaTeX installation. For debugging purposes."
    docker run -it --rm --volume "$(pwd)":/data --entrypoint=/bin/sh pandoc-docless:latest
}

do-view()
{
    echo "$SCRIPTNAME: Launching PDF viewer."
    xdg-open "$1".pdf &>/dev/null &
}

if [ $# -lt 1 ] ; then
    echo "$SCRIPTNAME: Error: No COMMAND given."
    exit $FAILURE
fi
cmd=$1
shift

echo "$SCRIPTNAME: Running: $cmd $@"
do-$cmd "$@"
exit $?
