# Doc-Less Monster (DLM)

A legendary framework to provide freedom from .doc/.docx files.

This project allows you to generate a PDF from a Markdown document,
applying LaTeX-based style to match your organization's templates.

## Getting Started

- To get the code, run: `git clone git@bitbucket.org:dcullen/doc-less-monster.git`

- Read the top-level `run.sh` and the files in the `docs/` directory.

- Run `./run.sh help` for basic command usage.

## License

This project is licensed under the [BSD 3-Clause License](LICENSE).
