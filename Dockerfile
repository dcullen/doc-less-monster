FROM pandoc/latex:2.14.0.2

# Install additional LaTeX packages that aren't provided in the standard pandoc/latex container.
RUN tlmgr install lastpage multirow

# Install additional fonts and related dependencies.
# - 'cm-super' allows many standard fonts like 'sans', 'helvet', and 'avant' to work correctly
#   (fixes "auto expansion is only possible with scalable fonts" error).
#   https://tex.stackexchange.com/questions/10706/pdftex-error-font-expansion-auto-expansion-is-only-possible-with-scalable
# - 'merriweather' and 'fontaxes' adds support for merriweather font as in this example:
#   https://tex.stackexchange.com/questions/417572/pandoc-set-sans-serif-family
RUN tlmgr install cm-super merriweather fontaxes
